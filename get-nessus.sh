#!/bin/bash

# Download Nessus package, and fix the name of the package according to
# Debian Policy [1], so that it can be used in live-build.
#
# [1] https://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-source

set -eu

NESSUS_VERSION=10.6.4
NESSUS_FILENAME=Nessus-${NESSUS_VERSION}-debian10_amd64.deb
NESSUS_URL=https://www.tenable.com/downloads/api/v2/pages/nessus/files/$NESSUS_FILENAME

wget "$NESSUS_URL" -O Nessus.deb

tmpdir=$(mktemp -d tmp.XXXXXX)
dpkg-deb --raw-extract Nessus.deb $tmpdir

cd $tmpdir/DEBIAN
pkg=$(sed -n 's/^Package: *//p' control)
arch=$(sed -n 's/^Architecture: *//p' control)
version=$(sed -n 's/^Version: *//p' control)
# fix package name, keeping only allowed characters: lower case letters (a-z),
# digits (0-9), plus (+) and minus (-) signs, and periods (.)
pkg=$(echo $pkg | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9+.-]*//g')
sed -i "s/^Package: .*/Package: $pkg/" control
cd $OLDPWD

dpkg-deb --build $tmpdir "${pkg}_${version}_${arch}.deb"
rm -fr $tmpdir

rm Nessus.deb
