Upload:

```
scp udd.py post.sh debian-ullmann:
```

Then run on ullmann:

```
./udd.py uploads-maintainer 'team\+pkg-go' > pkg-go.out
./post.sh pkg-go.out 2024
```
