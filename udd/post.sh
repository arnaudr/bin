#!/bin/bash

FILE=$1
YEAR=$2

DATA=$(grep ^$YEAR- $FILE)

echo "$DATA" | awk -F'\t' '{print $3}' | sort | uniq -c | sort -g
echo "TOTAL: $(echo "$DATA" | wc -l)"
