#!/usr/bin/python3

# https://udd.debian.org/schema/udd.html

import sys

import psycopg2

conn = psycopg2.connect("service=udd")
conn.set_client_encoding("UTF8")
cursor = conn.cursor()

# number of packages in a release
# REF: https://wiki.debian.org/UltimateDebianDatabase/

def n_source_packages(release):
    return f"""
    SELECT count(*) from sources where release='{release}'
    """

def print_n_source_packages(res):
    print(results[0][0])

# Example from:
# https://salsa.debian.org/qa/udd/-/blob/master/web/cgi-bin/last-uploads.cgi

_ = """
select changed_by_email, changed_by_name, date, source, version from upload_history
where (changed_by_name, changed_by_email, date) in (
select changed_by_name, changed_by_email, max(date)
from upload_history
group by changed_by_name, changed_by_email)
order by date desc
"""

# uploads by changed-by (ie. uploader)

def uploads_changed_by(pattern):
    return f"""
    select changed_by_email, maintainer_email, date, source, version from upload_history
    where changed_by_email ~ '{pattern}'
    group by changed_by_email, maintainer_email, date, source, version
    order by date desc
    """

def print_uploads_changed_by(res):
    for item in res:
        #print(item)
        changed_by, maintainer, date, source, version = item
        print(f"{date.strftime('%Y-%m-%d')}\t{changed_by:24}\t{maintainer:52}\t{source:48}\t{version}")
   
# uploads by maintainer

def uploads_maintainer(pattern):
    return f"""
    select changed_by_name, maintainer_email, date, source, version from upload_history
    where maintainer_email ~ '{pattern}'
    group by changed_by_name, maintainer_email, date, source, version
    order by date desc
    """

def print_uploads_maintainer(res):
    for item in res:
        #print(item)
        changed_by, maintainer, date, source, version = item
        print(f"{date.strftime('%Y-%m-%d')}\t{maintainer:32}\t{changed_by:32}\t{source:48}\t{version}")

# vcs by maintainer

def sources_maintainer(pattern):
    return f"""
    select maintainer_email, vcs_url from sources
    where maintainer_email ~ '{pattern}'
    group by maintainer_email, vcs_url
    """

def print_sources_maintainer(res):
    for item in res:
        #print(item)
        maintainer, vcs = item
        if not vcs: vcs = "undef"
        print(f"{maintainer:32}\t{vcs}")

# main

if sys.argv[1] == "n-src":
    release = sys.argv[2]   # sid, trixie, ...
    q = n_source_packages(release)
    print_res = print_n_source_packages
elif sys.argv[1] == "uploads-changed-by":
    pattern = sys.argv[2]   # arnaudr, kali\.org$, ...
    q = uploads_changed_by(pattern)
    print_res = print_uploads_changed_by
elif sys.argv[1] == "uploads-maintainer":
    pattern = sys.argv[2]   # team\+pkg-go, ...
    q = uploads_maintainer(pattern)
    print_res = print_uploads_maintainer
elif sys.argv[1] == "sources-maintainer":
    pattern = sys.argv[2]   # team\+pkg-security
    q = sources_maintainer(pattern)
    print_res = print_sources_maintainer

cursor.execute(q)
results = cursor.fetchall()
print_res(results)

if sys.argv[1].startswith(("uploads-", "sources-")):
    print(f"TOTAL: {len(results)}")
